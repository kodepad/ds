package a6_2dHourglass;

public class A6_1 {

    public static int hourglassSum(int [][] arr){

        int maxSum = Integer.MIN_VALUE;
        int sum = 0;
        for(int r = 0; r<=arr.length-3;r++)
        {
            for(int c = 0; c<=arr[0].length-3; c++)
            {
                sum = arr[r][c] + arr[r][c+1] + arr[r][c+2] + arr[r+1][c+1]
                        + arr[r+2][c] + arr[r+2][c+1] + arr[r+2][c+2];

                // Note : Update the sum just where its calculated, not outside of the for loop
                maxSum = Math.max(maxSum,sum);
            }
        }
        return maxSum;




    }
}
