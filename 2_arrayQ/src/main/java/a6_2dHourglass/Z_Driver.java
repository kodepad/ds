package a6_2dHourglass;

import java.util.Arrays;

import static a6_2dHourglass.A6_1.hourglassSum;

public class Z_Driver {

    //Question : https://www.hackerrank.com/challenges/2d-array/problem

    // Expected output = 19
    public static int [][] input1 = {
            {1,1,1,0,0,0},
            {0,1,0,0,0,0},
            {1,1,1,0,0,0},
            {0,0,2,4,4,0},
            {0,0,0,2,0,0},
            {0,0,1,2,4,0}
    };

    // Expected output = -6
    public static int[][] input2 = {
            {-1, -1, 0, -9, -2, -2},
            {-2, -1, -6, -8, -2, -5},
            {-1, -1, -1, -2, -3, -4},
            {-1, -9, -2, -4, -4, -5},
            {-7, -3, -3, -2, -9, -9},
            {-1, -3, -1, -2, -4, -5}
    };

    // Expected output = -19
    public static int[][] input3 = {
            { 0,-4,-6, 0,-7,-6},
            {-1,-2,-6,-8,-3,-1},
            {-8,-4,-2,-8,-8,-6},
            {-3,-1,-2,-5,-7,-4},
            {-3,-5,-3,-6,-6,-6},
            {-3,-6, 0,-8,-6,-7}
    };

    public static void main(String[] args) {

        System.out.println("Printing columns");
        System.out.println(Arrays.toString(input1[0]));
        System.out.println(Arrays.toString(input1[2]));

        System.out.println("Number of rows in Matrix");
        System.out.println(input1.length);
        System.out.println("Number of columns in Matrix");
        System.out.println(input1[0].length);

        System.out.println("Running Code");
        System.out.println(hourglassSum(input1));
        System.out.println(hourglassSum(input2));
        System.out.println(hourglassSum(input3));

    }
}
