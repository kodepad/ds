package a7_numberSum;

import java.util.Arrays;

import static a7_numberSum.A7_1_TwoSum.twoNumberSum;

public class Z_Driver {

    public static void main(String[] args) {

        int [] arr = {5, 4,6,1,-3, 2};
        int targetSum = 10;

        System.out.println(Arrays.toString(twoNumberSum(arr,targetSum)));

    }
}
