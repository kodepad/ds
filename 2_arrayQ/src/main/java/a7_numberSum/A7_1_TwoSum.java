package a7_numberSum;

import java.util.HashMap;
import java.util.Map;

public class A7_1_TwoSum {

    // targetSum = 7, {3,1,2,4}
   // public static int[] twoNumberSum(int[] array, int targetSum){

    public static int [] twoNumberSum(int[] array, int targetSum){

        Map<Integer, Integer> map = new HashMap();

        // populate

        // because value at array index is unique, using it as key
        for(int index = 0; index < array.length; index ++)
        {
            int target = targetSum - array[index];

            if(map.containsKey(target))
            {
                return new int[] {array[index], array[map.get(target)]};
            }
            else {
                map.put(array[index], index);
            }
        }
        return new int[]{};
    }
}
