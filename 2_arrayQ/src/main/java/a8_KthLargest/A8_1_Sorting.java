package a8_KthLargest;

import java.util.Arrays;

/*
Leetcode : 215
 */
public class A8_1_Sorting {


    public static int findKthLargestSorting(int[] array, int k)
    {
        /*
        Complexity : O(n logn)
         */
        System.out.println("Input array" + Arrays.toString(array));
        Arrays.sort(array);
        System.out.println("Sorted array" + Arrays.toString(array));
        /*
         Note : to find the kth largest number, we have to remove k-1 element from sorted array
         that index would be (n-1) - (k-1)
        */
        return array[array.length - k];
    }
}
