package a8_KthLargest;

import java.util.Arrays;
import java.util.PriorityQueue;


public class A8_2_MinHeap {

    public static int findKthLargestMinHeap(int[] array, int k){

        System.out.println("Input array" + Arrays.toString(array));

        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for(int i : array)
        {
            minHeap.add(i);
            if(minHeap.size() > k)
            {
                minHeap.remove();
            }
        }
        return minHeap.remove();
    }
}
