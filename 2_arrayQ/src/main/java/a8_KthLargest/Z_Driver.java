package a8_KthLargest;

import static a8_KthLargest.A8_1_Sorting.findKthLargestSorting;
import static a8_KthLargest.A8_2_MinHeap.findKthLargestMinHeap;

public class Z_Driver {

    public static void main(String[] args) {
        int[] arr1 = {3,2,1,5,6,3};
        int[] arr2 = {3,2,3,1,2,4,5,5,6};
        int k1 = 5;
        int k2 = 4;


        System.out.println( k1 + "th largest element : " + findKthLargestSorting(arr1, k1));
        System.out.println("\n\n");
        System.out.println( k2 + "th largest element : " + findKthLargestSorting(arr2, k2));


        int[] arr3 = {3,2,1,5,6,3};
        int[] arr4 = {3,2,3,1,2,4,5,5,6};
        int k3 = 5;
        int k4 = 4;
        System.out.println("\n\n");
        System.out.println( k1 + "th largest element : " + findKthLargestMinHeap(arr3, k3));
        System.out.println("\n\n");
        System.out.println( k2 + "th largest element : " + findKthLargestMinHeap(arr4, k4));

    }
}
