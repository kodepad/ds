package other;

import java.util.HashMap;
import java.util.Map;

public class Demo3 {

    public static void main(String[] args) {

        String s = "cbbebi";
        int k = 3;

        System.out.println(findLen(s, k));

    }

    private static int findLen(String s, int k) {
        Map<Character, Integer> freqMap = new HashMap<>();
        int maxLength = Integer.MIN_VALUE;

        // not a fixed size window

        int startPointer = 0;

        for (int endPointer = 0; endPointer < s.length() ; endPointer++){
            char rightChar = s.charAt(endPointer);


            freqMap.put(rightChar, freqMap.getOrDefault(rightChar, 0)+1);


            while(freqMap.size() > k){
                char leftChar = s.charAt(startPointer);
                freqMap.put(leftChar, freqMap.get(leftChar) - 1);
                if(freqMap.get(leftChar) == 0) {
                    freqMap.remove(leftChar);  // left char
                }
                startPointer++;
            }
            maxLength = Math.max(maxLength, endPointer - startPointer + 1);
        }

        return maxLength;
    }
}


/*
a - 3
r - 1
c
 */