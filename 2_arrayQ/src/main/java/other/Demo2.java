package other;

import java.util.Arrays;

public class Demo2 {

    public static void main(String[] args) {
        int[] arr1 = {2, 1, 5, 2, 3, 2};
        int[] arr2 = {2, 1, 5, 2, 8};
        int[] arr3 = {3, 4, 1, 1, 6};

        int k1 = 7;
        int k2 = 7;
        int k3 = 8;

        int result1 = smallestSubArray(arr1, k1);
        System.out.println((result1));

        int result2 = smallestSubArray(arr2, k2);
        System.out.println((result2));

        int result3 = smallestSubArray(arr3, k3);
        System.out.println((result3));
    }

    private static int smallestSubArray(int[] arr, int k) {

        // window of variable size

        int sum = 0;
        int startPointer = 0;
        int minimumValue = Integer.MAX_VALUE;
        int subArrayLenght = 0;

        for (int endPointer =0; endPointer < arr.length; endPointer++){  // for loop -> incrementing the window
            sum = sum + arr[endPointer];

            while(sum >= k)   // -- shrinking the window
            {
                // length find
                // min size
                // return

                subArrayLenght = endPointer - startPointer + 1;
                minimumValue =  Math.min(subArrayLenght, minimumValue);

                sum = sum - arr[startPointer];
                startPointer++;
            }
        }
            return minimumValue;
    }
}
