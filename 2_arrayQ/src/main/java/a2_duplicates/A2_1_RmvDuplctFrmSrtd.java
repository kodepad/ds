package a2_duplicates;

/* Question :
1. Remove duplicate from Sorted Array In Place
2. Leetcode # 26

 */

import java.util.Arrays;

public class A2_1_RmvDuplctFrmSrtd {

    private static int[] arr = {1,2,2,4,5,6,6,7,8,8};

    public static void main(String[] args) {
        System.out.println(Arrays.toString(removeDuplicate(arr)));
    }

    private static int[] removeDuplicate(int[] arr) {
        /* Note :
         Where should we place the new number if it unique
         First number is always unique, so index will start with 1
          */

        int index = 1;

        for (int i =0; i < arr.length-1; i++)
        {
            if(arr[i] != arr[i+1])
            {
                arr[index++] = arr[i + 1];
            }
        }
        return Arrays.copyOf(arr,index);
    }
}