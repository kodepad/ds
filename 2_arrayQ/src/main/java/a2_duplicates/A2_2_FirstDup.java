package a2_duplicates;

// TODO : https://www.youtube.com/watch?v=XSdr_O-XVRQ&t=259s


import java.util.HashSet;
import java.util.Set;

public class A2_2_FirstDup {


    public static int setMethod(int [] arr) {
        Set<Integer> seen = new HashSet<>();

        for(int i = 0; i < arr.length; i++){
            if(seen.contains(arr[i]))
                return arr[i];
            seen.add(arr[i]);
        }
        return -1;
    }

/*   TODO
    public static int noExtraSpace(int[] arr){

    }*/
}
