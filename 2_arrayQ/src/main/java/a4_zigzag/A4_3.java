package a4_zigzag;

public class A4_3 {

    public static int [] createZigZagArray(int[] arr){

        for(int i =1; i < arr.length-1; i=i+2)
        {
            if(arr[i] > arr[i-1])
            {
                swap(arr,i,i-1);
            }

            if(arr[i] > arr[i+1])
            {
                swap(arr,i,i+1);
            }
        }
        return arr;
    }

    private static void swap(int[] arr, int i , int j)
    {
        int temp;
        temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
    }
}
