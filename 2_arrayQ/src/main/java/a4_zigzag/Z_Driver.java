package a4_zigzag;

import java.util.Arrays;

public class Z_Driver {

    public static void main(String[] args) {


        int[] input1 = {7, 6, 10, 12, 1, 0, 13, 14};
        int[] input2 = {4,3,7,8,6,2};

        int[] output1 = A4_1.createZigZagArray(input1.clone());
        System.out.println(Arrays.toString(output1));

        int[] output2 = A4_2_Soln.createZigZagArray(input1.clone());
        System.out.println(Arrays.toString(output2));

        int[] output3 = A4_1.createZigZagArray(input2.clone());
        System.out.println(Arrays.toString(output3));

        int[] output4 = A4_2_Soln.createZigZagArray(input2.clone());
        System.out.println(Arrays.toString(output4));

        // Note : This scenario is for where the first element is greater than the second and so on

        int[] output5 = A4_3.createZigZagArray(input2.clone());
        System.out.println(Arrays.toString(output5));
    }
}
