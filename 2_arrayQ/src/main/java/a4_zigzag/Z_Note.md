## ZigZag Array

* zigzag if no three consecutive elements in the array are either increasing or decreasing.  
* https://www.hackerrank.com/contests/world-codesprint-10/challenges/zigzag-array


### First Approach  
* Sort the Array
* Then swap every two integers after the first integer  
* https://medium.com/@domarp/sorting-an-array-of-integers-into-a-zig-zag-pattern-c59507915cb


### Second approach
* A zigzag array would look like  

        a < b > c < d > e < f > g < h
        
* Every element at even position is greater than its adjacent elements
* Hence, compare the elements at the even position and if its not greater that its adjacent elements, swap them
* https://www.youtube.com/watch?v=5mNTvy8Bzuc

### Third approach
* A zigzag array can also look like  

        a > b < c > d < e > f < g > h 
* In that case, even elements would be smaller than both adjacent elements.


#### Note : The solution for zigzag array is not unique