package a4_zigzag;

import java.util.Arrays;

public class A4_1 {


    public static int [] createZigZagArray(int[] arr)
    {
        Arrays.sort(arr);
        // Note : Make sure i is incereasing with +2 and not +1
        for(int i = 1; i < arr.length-1; i = i+2){
            swap(arr, i, i+1);
        }
        return arr;
    }

    private static void swap(int[] arr, int i, int j)
    {
        int temp;
        temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
    }
}
