package a3_rotation;

public class A3_2_LeftRotaion {

    // Reversal Algorithm
    static int[] rotateLeft(int[] a, int d) {

        reverseArray(a,0,d-1);
        reverseArray(a,d,a.length-1);
        reverseArray(a,0,a.length - 1);
        return a;
    }

    private static void reverseArray(int [] arr, int startIndex, int endIndex){
        while (startIndex < endIndex) {
            swap(arr, startIndex, endIndex);
            startIndex++;
            endIndex--;
        }
    }

    private static void swap(int[] arr, int i, int j)
    {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

}
