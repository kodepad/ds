package a1_reversal;

import java.util.Arrays;

public class Z_Driver {

    // https://www.youtube.com/watch?v=nGLiMx5mLYY

    public static void main(String[] args) {

        // TODO : Failed, check the last group condition

        int[] arr1 = {1, 2, 3, 4, 5, 6, 7, 8};
        int[] arr2 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int k = 3;

        int[] output1 = A1_2_GroupReversal.reverseGroup(arr1.clone(),k);
        int[] output2 = A1_2_GroupReversal.reverseGroup(arr2.clone(),k);
        System.out.println(Arrays.toString(output1));
        System.out.println(Arrays.toString(output2));
    }
}
