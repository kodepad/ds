package a1_reversal;

public class A1_2_GroupReversal {

    public static int[] reverseGroup(int[] arr, int k) {


        for(int i =0; i < arr.length; i = i + k )
        {
            int left = i;

            // Note : To handle the last group
            int right = Math.min(arr.length - 1, i + k - 1 );
            reverseArray(arr, left, right);

        }
        return arr;
    }

    private static void reverseArray(int [] arr, int startIndex, int endIndex){
        while (startIndex < endIndex) {
            swap(arr, startIndex, endIndex);
            startIndex++;
            endIndex--;
        }
    }

    private static void swap(int[] arr, int i, int j)
    {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}