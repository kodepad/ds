package a1_reversal;

// Question : Reverse an array in memory

import java.util.Arrays;

public class A1_1_InMemoryArrayReversal {

    private static int[] oddArray = {4,2,7,9,3,5,1};
    private static int[] evenArray = {4,2,6,8};

    public static void main(String[] args) {
        reverseArrayInPlace(oddArray);
        System.out.println(Arrays.toString(oddArray));
        reverseArrayInPlace(evenArray);
        System.out.println(Arrays.toString(evenArray));
    }

    private static void reverseArrayInPlace(int[] arr) {

        // NOTE : Always pay attention to length - 1 in case of array and string

        int i = 0;
        int j = arr.length - 1;

        while(i < j)
        {
            swap(i,j,arr);
            i++;
            j--;
        }
    }

    public static void swap(int i, int j, int[] arr){
        int temp;

        temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }


}
