## Questions / Topics

* How does heap work ?
    * Max Heap always keeps track of maximum element at its root
* Priority queue implementation of the same
* Difference between remove() and poll() of queue
* How to sort a map, by keys and values   
    * top k frequent elements
    
    
* https://www.educative.io/edpresso/min-heap-vs-max-heap
