###Arrays.sort(int[]) 
* It is always in place.
    * Next time the array is used, it would already be sorted
* Avg Time complexity of `n logn`
* Worst Time complexity of `n^2`
* Space complexity : O(1), as its all in place

* https://stackoverflow.com/questions/22571586/will-arrays-sort-increase-time-complexity-and-space-time-complexity
* https://www.baeldung.com/arrays-sortobject-vs-sortint