### Arrays / String   
1. Always pay attention to length - 1 in case of array and string    
2. array.length and string.length()  
3. String class in Java does not have reverse() method, however StringBuilder class has built in reverse() method.  
4. StringBuilder class do not have toCharArray() method, while String class does have toCharArray() method. 

### List  
1. list.add()  
2. list.get(int)  
3. Size and index of list will have a difference of 1  
  
### Common mistakes  
1.   Don't forget to give diamond operator while declaring a generic      

    `Warning: Unchecked assignment: java.util.LinkedList' to 'java.util.List<java.lang.String>`

     
Replace `List<String> lst = new LinkedList();` with `List<String> lst = new LinkedList<>();`


### Map
1. While creating a frequency table, dont forget to use default value  

        for(int i : arr){
        m1.put(i, m1.getOrDefault(i, 0) + 1);
        
2. Not using default will throw NPE in case the key is not present in map

3. To add elements into a List inside a map, below lambda expression can be used, whereing we dont need to check for `putIfAbsent` or `getOrDefault`   

          countMap.computeIfAbsent(empRecord[0], k -> new ArrayList<>()).add(val);
          
4. Need to check if point 3 can be used for point 2