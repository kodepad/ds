### Integer
* Integer.MIN_VALUE is negative
* Integer.MAX_VALUE is positive
* There is a difference of one between them



### Double
* Double.MIN_VALUE is a positive number
* Minimum value a Double can take is  -Double.MAX_VALUE
* Magnitude of both MAX_VALUE and MIN_VALUE is same as they are symmetrical around origin. It has first bit for sign, 



### Refernce

https://stackoverflow.com/questions/3884793/why-is-double-min-value-in-not-negative