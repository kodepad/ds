/*
1. Create a new array from an old array
2. https://stackoverflow.com/questions/8193402/creating-new-array-with-contents-from-old-array-while-keeping-the-old-array-stat
3. https://www.softwaretestinghelp.com/java-copy-array/
4. https://www.tutorialspoint.com/Array-Copy-in-Java
 */

package arrays;

import java.util.Arrays;

public class NewArray {

    private static int[] arr = {1,2,3,4,5,6,7,8,9};

    public static void main(String[] args) {
        System.out.println(Arrays.toString(method1()));
        System.out.println(Arrays.toString(method2()));
        System.out.println(Arrays.toString(method3()));
    }

    private static int[] method3() {
        // Copy from and to given indexes
        return Arrays.copyOfRange(arr, 3,7);
    }

    private static int[] method2() {
        // Copies n elements, truncating the rest
        return Arrays.copyOf(arr,4);
    }

    private static int[] method1()
    {
        /*
            Using System.arrcopy()
            The method takes five arguments:
                src: The source array.
                srcPosition: The position in the source from where you wish to begin copying.
                des: The destination array.
                desPosition: The position in the destination array to where the copy should start.
                length: The number of elements to be copied.
         */

        int[] newArr = new int[10];
        System.arraycopy(arr,3, newArr, 2,6);
        return newArr;
    }
}
