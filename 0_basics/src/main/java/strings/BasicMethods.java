package strings;

public class BasicMethods {

    private static String str = "How are you";

    public static void main(String[] args) {
        split();
        stringAndcharArray();
        substring();
    }


    private static void split() {
        String[] strArr = str.split(" ");

        for (String s : strArr) {
            System.out.println(s);
        }
    }

    private static void stringAndcharArray() {

        String input = "Coding";

        char temp;
        int l = 0;
        int r = input.length()-1;
        // NOTE : Converting String to CharachterArray
        char [] charArr = input.toCharArray();
        temp = charArr[l];
        charArr[l] = charArr[r];
        charArr[r] = temp;

        // Note : Converting charachter array back to String
        System.out.println( String.valueOf(charArr));
    }

    private static void substring() {

        // Caveat : Substring
        String s = "ABCDEF";

        System.out.println(s.length());

        // Note : Substring from beginning to last - 1
        System.out.println(s.substring(0,2));

        // Note : From index to end
        System.out.println(s.substring(2));

        //                     AB               Z       CDEF
        System.out.println(s.substring(0,2) + 'Z' + s.substring(2));
    }
}

