package lists;

import java.util.LinkedList;
import java.util.List;

public class Basics {

    public static void main(String[] args) {

        List<String> lst = new LinkedList();
        lst.add("Hello");
        lst.add("World");

        System.out.println(lst.size());
        System.out.println(lst.get(0));
        System.out.println(lst.get(1));


        try {
            System.out.println(lst.get(lst.size()));
        }
        catch (IndexOutOfBoundsException e)
        {
            System.out.println("Size and index of list will have a difference of 1");
        }
    }
}
