package s7_subsequence;

public class S7_2_IterateMainString {

    public static boolean isSubsequence(String subsequence, String str) {
        int innerIdx = 0;

        for (int outerIdx = 0; outerIdx < str.length(); outerIdx++) {
            if (subsequence.charAt(innerIdx) == str.charAt(outerIdx)) {
                innerIdx++;
                // NOTE : Out of bound exception in case the last element of subsequence is matched and innerIndex is increased
            }
            if (innerIdx == subsequence.length()) {
                return true;
            }
        }
        return false;
    }
}


