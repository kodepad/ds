package s7_subsequence;

/*
# LeetCode 392. Is Subsequence

 */
public class S7_1_IterateSubsequence {

    public static boolean isSubsequence(String subsequence, String str) {
        int innerIdx = 0;
        int outerIdx = 0;

        while (innerIdx < subsequence.length() && outerIdx < str.length()) {
            if (subsequence.charAt(innerIdx) == str.charAt(outerIdx)) {
                // Note : index = 1 at first match, so n+1 after n matches
                innerIdx++;
            }
            outerIdx++;
        }
        return innerIdx == subsequence.length();
    }
}

