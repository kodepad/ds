package s7_subsequence;

public class SubsequenceInArray {

    public static int numberOfMatchingSubsequence(String str, String[] words){
        int count = 0;
        for(String s : words)
        {
            if(new S7_1_IterateSubsequence().isSubsequence(s,str))
                count++;
        }
        return count;
    }

    public static void main(String[] args) {
        String s;
        s = "abcde";
        String[] words1 = {"a","bb","acd","ace"};
        System.out.println(numberOfMatchingSubsequence(s, words1));
    }


}



