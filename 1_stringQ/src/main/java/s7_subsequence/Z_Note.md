## AlgoExpert : Validate Subsequence
## LeetCode 392. Is Subsequence

Given two strings s and t, check if s is a subsequence of t.

A subsequence of a string is a new string that is formed from the original string by deleting some (can be none) of the characters without disturbing the relative positions of the remaining characters. (i.e., "ace" is a subsequence of "abcde" while "aec" is not).


## 792. Number of Matching Subsequences
Given a string s and an array of strings words, return the number of words[i] that is a subsequence of s.

SubsequenceInArray - time limit exceeded


## Points to remember

* Notes in code
* Inner Index = 1 after first match, so it will be n+1 after nth match, and hence equating it to length() and not length() - 1
* While iterating outer loop, addition if condition on length check, as if the last element of inner index is matched, its value would be increased by 1 and will cause Out Of Bound in next iteration

