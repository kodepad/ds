package s7_subsequence;


public class Z_Driver {

    public static void main(String[] args) {

        String s1 = "abc";
        String t1 = "ahbgdc";

        String s2 = "axc";

        System.out.println("Iterating sequence");
        System.out.println(S7_1_IterateSubsequence.isSubsequence(s1,t1));
        System.out.println(S7_1_IterateSubsequence.isSubsequence(s2,t1));


        System.out.println("Iterating main string");
        System.out.println(S7_2_IterateMainString.isSubsequence(s1,t1));
        System.out.println(S7_2_IterateMainString.isSubsequence(s2,t1));
    }
}
