package s1_reverseString;

public class Driver {


    public static void main(String[] args) {

        String input = "Learn Data Structure";

        System.out.println(S1_1.reverseString(input));
        System.out.println(S1_2.reverseString(input));
        System.out.println(S1_3.reverseString(input));
        System.out.println(S1_4.reverseString(input));
    }

}
