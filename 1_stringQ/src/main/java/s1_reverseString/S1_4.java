package s1_reverseString;

import org.apache.commons.lang3.StringUtils;

public class S1_4 {

    public static String reverseString(String input) {
        return StringUtils.reverse(input);
    }

}
