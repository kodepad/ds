package s1_reverseString;

import org.apache.commons.lang3.StringUtils;

/* Question
1. Reverse a String
2. Leetcode #344

 */
public class S1_1 {







    public static String reverseString(String input) {
        char[] chars = input.toCharArray();
        int i = 0;
        int j = input.length() - 1;
        char temp;

        while (i < j) {
            temp = chars[i];
            chars[i] = chars[j];
            chars[j] = temp;
            i++;
            j--;
        }
        return new String(chars);
    }
}
