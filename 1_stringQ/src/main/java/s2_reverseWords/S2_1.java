package s2_reverseWords;

import java.util.Arrays;

public class S2_1 {


    public static String reverseSentence(String input) {
        String [] sArr = input.split(" ");
        int i = 0;
        int j = sArr.length - 1;
        String temp;

        while( i < j)
        {
            temp = sArr[i];
            sArr[i] = sArr[j];
            sArr[j] = temp;
            i++;
            j--;
        }
        return Arrays.toString(sArr);
    }
}
