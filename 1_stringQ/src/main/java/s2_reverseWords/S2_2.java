package s2_reverseWords;

public class S2_2 {

    public static String reverseSentence(String input) {
        String sArr[] = input.split(" ");
        StringBuilder sb = new StringBuilder();

        for(int i = sArr.length - 1; i >=0; i--)
        {
            sb.append(sArr[i]);
            sb.append(" ");
        }
        return sb.toString();
    }
}
