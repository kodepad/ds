package s2_reverseWords;


public class Driver {

    private static String input = "How are you doing today ?";

    public static void main(String[] args) {

        System.out.println(input.split(" "));
//        System.out.println(Arrays.toString(input.split(" ")));
        String output_1 = S2_1.reverseSentence(input);
        String output_2 = S2_2.reverseSentence(input);
        String output_3 = S2_3.reverseSentence(input);
        System.out.println(output_1);
        System.out.println(output_2);
        System.out.println(output_3);
    }





}
