package s2_reverseWords;

import org.apache.commons.lang3.StringUtils;

public class S2_3 {

    public static String reverseSentence(String input) {
        return StringUtils.reverseDelimited(input, ' ');
    }

}
