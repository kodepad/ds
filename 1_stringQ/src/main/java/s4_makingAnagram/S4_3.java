package s4_makingAnagram;

import java.util.Arrays;

public class S4_3 {

    static int makeAnagram(String first, String second) {
        int[] freq = new int[26];

        first.chars().forEach(c -> {
            freq[c - 'a']++;
        });

        second.chars().forEach(c -> {
            freq[c - 'a']--;
        });

        return Arrays.stream(freq).map(Math::abs).sum();
    }
}
