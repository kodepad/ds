package s4_makingAnagram;


import java.util.Arrays;

public class S4_2 {



    static int makeAnagram(String a, String b) {

        if (a.length() == 0 || b.length() == 0)
            return a.length() == 0 ? b.length() : a.length();


        int[] count = new int[256];
        char[] ch1 = a.toCharArray();
        char[] ch2 = b.toCharArray();
        int result = 0;

        Arrays.fill(count,0);


        for(int i =0; i < a.length() ; i++)
        {
            count[ch1[i]]++;
        }

        for(int i =0; i < b.length(); i++)
        {
            count[ch2[i]]--;
        }

        for(int i =0; i < 256; i++)
        {
            result += Math.abs(count[i]);
        }
        return result;
    }
}
