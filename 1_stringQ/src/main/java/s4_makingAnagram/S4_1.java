package s4_makingAnagram;

import java.util.Arrays;

public class S4_1 {

    static int makeAnagram(String a, String b) {
        int[] count1 = new int[256];
        int[] count2 = new int[256];

        char[] ch1 = a.toCharArray();
        char[] ch2 = b.toCharArray();
        int del = 0;

        Arrays.fill(count1,0);
        Arrays.fill(count2,0);

        for(int i =0; i < a.length() ; i++)
        {
            count1[ch1[i]]++;
        }

        for(int i =0; i < b.length(); i++)
        {
            count2[ch2[i]]++;
        }

        for(int i =0; i < 256; i++)
        {
            if(count1[i]!=count2[i])
            {
                del = del + Math.abs(count1[i]-count2[i]);
            }
        }
        return del;
    }

}
