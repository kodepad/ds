package s4_makingAnagram;


public class Driver {

    // https://www.hackerrank.com/challenges/ctci-making-anagrams

    public static void main(String[] args) {
        String a = "cde";
        String b = "abc";

        String x = "fcrxzwscanmligyxyvym";
        String y = "jxwtrhvujlmrpdoqbisbwhmgpmeoke";

        System.out.println(S4_1.makeAnagram(a,b));
        System.out.println(S4_1.makeAnagram(x,y));

        System.out.println(S4_2.makeAnagram(a,b));
        System.out.println(S4_2.makeAnagram(x,y));

        System.out.println(S4_3.makeAnagram(a,b));
        System.out.println(S4_3.makeAnagram(x,y));
    }
}
