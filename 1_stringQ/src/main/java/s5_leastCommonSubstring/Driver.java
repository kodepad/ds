package s5_leastCommonSubstring;

public class Driver {

    public static void main(String[] args) {

        String input1 = "aardvark";
        String input2 = "apple";
        String input3 = "beetroot";
        String input4 = "sandals";
        String input5 = "hackerrankcommunity";
        String input6 = "cdecdecdecde";
        String input7 = "jackandjill";
        String input8 = "wentupthehill";
        String input9 = "hello";
        String input10 = "world";
        String input11 = "hi";
        String input12 = "world";


        System.out.println(S5_1.leastCommonSubstring(input1,input2));
        System.out.println(S5_1.leastCommonSubstring(input3,input4));
        System.out.println(S5_1.leastCommonSubstring(input5, input6));
        System.out.println(S5_1.leastCommonSubstring(input7, input8));
        System.out.println(S5_1.leastCommonSubstring(input9,input10));
        System.out.println(S5_1.leastCommonSubstring(input11,input12));
    }
}
