package s5_leastCommonSubstring;

import java.util.Arrays;

public class S5_1 {

    // Crude bitmap

    public static String leastCommonSubstring(String s1, String s2) {
        int[] arr1 = new int[26];
        int[] arr2 = new int[26];
        // TODO : Even if assigned later, why does it show as can be null
        String op = "NO";

        // TODO : Do we need arrays.fill here ? Why is above array initialized to 0

        Arrays.fill(arr1, 0);
        Arrays.fill(arr2, 0);

        populateBitMaps(s1, arr1);
        populateBitMaps(s2, arr2);

        for (int i = 0; i < 26; i++) {
            if (arr1[i] == 1 && arr2[i] == 1) {
                op = "YES";
                break;
            } else {
                op = "NO";
                // Using break here would break at first occurrence of this condition, hence use break only when yes condition is met
            }
        }
        return op;
    }

    private static void populateBitMaps(String s, int[] arr) {
        for (int i = 0; i < s.length(); i++) {
            arr[s.charAt(i) - 'a'] = 1;
        }
    }

}
