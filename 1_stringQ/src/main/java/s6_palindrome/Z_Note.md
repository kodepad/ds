### Palindrome
        
* Any string containing just one letter is by default a palindrome
* An empty string is also a palindrome, since it "reads" the same forward and backward
* Base Case - A string with exactly zero letters or one letter is a palindrome



### Pseudocode
##### Base Condition 
* If the string is made of no letters or just one letter, then it is a palindrome. Otherwise, compare the first and last letters of the string.

##### Recursion
* If the first and last letters differ, then the string is not a palindrome. 
* If the first and last letters are the same. Strip them from the string, and determine whether the string that remains is a palindrome. 
* Take the answer for this smaller string and use it as the answer for the original string.
     
     
## Points to remember

* While working with string and esp substring, make sure to use length() - 1
* A recursive method return itself, not only call itself
* Recursive methods must have a base condition