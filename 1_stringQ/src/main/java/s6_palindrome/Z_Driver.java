package s6_palindrome;

public class Z_Driver {

    public static void main(String[] args) {

        String str1 = "MALAALAM";
        String str2 = "MALAYALAM";
        String str3 = "ABCBA";
        String str4 = "ABCDA";

        System.out.println("Iterative");

        System.out.println(S6_1_Iterative.isPalindrome(str1));
        System.out.println(S6_1_Iterative.isPalindrome(str2));
        System.out.println(S6_1_Iterative.isPalindrome(str3));
        System.out.println(S6_1_Iterative.isPalindrome(str4));

        System.out.println("Recursive");

        System.out.println(S6_2_Recursive.isPalindrome(str1));
        System.out.println(S6_2_Recursive.isPalindrome(str2));
        System.out.println(S6_2_Recursive.isPalindrome(str3));
        System.out.println(S6_2_Recursive.isPalindrome(str4));
    }
}
