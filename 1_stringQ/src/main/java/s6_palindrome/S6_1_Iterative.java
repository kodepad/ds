package s6_palindrome;

public class S6_1_Iterative {

    public static boolean isPalindrome(String str)
    {
        // Note : Always handle edge cases
        if(str.length() == 0 || str.length() == 1 )
            return true;

        for(int i =0; i < str.length()/2; i ++)
        {
            // NOTE : For i = 0, length() would throw error
            if(str.charAt(i)!=str.charAt(str.length()-1-i)){
                return false;
            }
        }
        return true;
    }
}
