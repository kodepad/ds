package s6_palindrome;

public class S6_2_Recursive {


    public static boolean isPalindrome(String str){

        if(str.length() == 0 || str.length() == 1)
            return true;

        if(str.charAt(0)==str.charAt(str.length()-1)){

            // Note : Substring extend to endIndex - 1
            String newStr = str.substring(1, str.length() - 1);
            return isPalindrome(newStr);
        }
        return false;
    }
}
