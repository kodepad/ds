package s8_freedom;

import java.util.*;

public class EntryAndExit {

    public static Map<String, List<String>> unpairedEntryExit_method1(String[][] entryLog) {
        Map<String, List<Integer>> countMap = new HashMap<>();
        // Caveat : Do not use same method variable as value for multiple key in a map
        //  List<Integer> lst = new LinkedList<>();
        Map<String, List<String>> resultMap = new HashMap<>();
        List<String> exitList = new ArrayList<>();
        List<String> entryList = new ArrayList<>();

        int val = 0;

        for (String[] empRecord : entryLog) {
            // Caveat : Use this approach when value in a map is a collection
            countMap.put(empRecord[0], new LinkedList<>());
        }
        System.out.println("Initial Map " + countMap);

        for (String[] empRecord : entryLog) {
            if (empRecord[1].equals("exit"))
                val = 1;
            else if (empRecord[1].equals("entry"))
                val = -1;

            countMap.get(empRecord[0]).add(val);
        }

        printMap(countMap);


        for (String empName : countMap.keySet()) {

            /*
            Note : Without Lambda
            Need of another for loop to iterate the list : O (n^2)
             */
            int sum = 0;
            for(int i =0; i < countMap.get(empName).size(); i++)
            {
                sum = sum + countMap.get(empName).get(i);
            }


            if (sum < 0) {
                entryList.add(empName);

                // Question : Why does this kind of expression not work.
                // Note : Above one does not work because its expecting a String and LinkedList and not a boolean
                // Question :  Why List.add() returns boolean ?

                //  TODO Check code with this expression : resultMap.put("Entry", resultMap.getOrDefault("Entry", new LinkedList<>()).add(empName));
                // TODO : Clean the markdown file
            } else if (sum > 0) {
                exitList.add(empName);
            }
        }
        resultMap.put("Entry", entryList);
        resultMap.put("Exit", exitList);

        return resultMap;
    }




    public static Map<String, List<String>> unpairedEntryExit_method2(String[][] entryLog) {
        Map<String, List<Integer>> countMap = new HashMap<>();
        Map<String, List<String>> resultMap = new HashMap<>();

        int val = 0;

        for (String[] empRecord : entryLog) {
            if (empRecord[1].equals("exit"))
                val = 1;
            else if (empRecord[1].equals("entry"))
                val = -1;

            // Note : Use of lambda removed the need to populate initial map
            // Note : Make a note of it
            countMap.computeIfAbsent(empRecord[0], k -> new ArrayList<>()).add(val);
        }



        for (String empName : countMap.keySet()) {

            //TODO : Lambda, find the time complexity of this
            int i = countMap.get(empName).stream().mapToInt(Integer::intValue).sum();

            if (i < 0) {
               resultMap.computeIfAbsent("Entry", k -> new ArrayList<>()).add(empName);

            } else if (i > 0) {
                resultMap.computeIfAbsent("Exit", k -> new ArrayList<>()).add(empName);
            }
        }
        return resultMap;
    }


    private static void printMap(Map map) {
        // Note : Just to check map
        System.out.println("Map " + map);
        System.out.println("Map keySet : " + map.keySet());
        System.out.println("Map valueSet : " + map.values());
        System.out.println("Map entrySet : " + map.entrySet());
    }



    }
