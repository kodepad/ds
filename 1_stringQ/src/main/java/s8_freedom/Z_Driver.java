package s8_freedom;

import static s8_freedom.EntryAndExit.*;

public class Z_Driver {

    public static void main(String[] args) {

        String[][] table = {
                {"A", "entry"},
                {"B", "entry"},
                {"A", "exit"},
                {"A", "entry"},
                {"A", "exit"},
                {"A", "exit"},
                {"B", "exit"},
                {"B", "entry"},
                {"C", "entry"},
                {"D", "entry"},
                {"C", "exit"},
                {"C", "entry"},
                {"D", "exit"}
        };

        System.out.println("Without Lambda \n");

        System.out.println(unpairedEntryExit_method1(table));

        System.out.println("\n\n With Lambda \n");
        System.out.println(unpairedEntryExit_method2(table));

    }
}
