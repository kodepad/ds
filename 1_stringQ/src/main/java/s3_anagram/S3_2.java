package s3_anagram;

import java.util.Arrays;

public class S3_2 {

    /*
        Use Sorting
            Check length
            Sort both of them
            Compare char Array
 */


    public static boolean isAnagram(String input1, String input2) {

        if(input1.length()!= input2.length())
            return false;

        // Note : No need to compare the case of letter here

        char[] ch1 = input1.toCharArray();
        char[] ch2 = input2.toCharArray();

        Arrays.sort(ch1);
        Arrays.sort(ch2);

        for(int i =0; i < input1.length(); i++)
        {
            if(ch1[i]!=ch2[i])
                return false;
        }
        return true;
    }

}
