package s3_anagram;

public class Driver {

        public static void main(String [] args)
    {
//        String input1 = "ABCYT";
//        String input2 = "CABTY";

        String input1 = "AAB";
        String input2 = "ABA";
        System.out.println(S3_1.isAnagram(input1,input2));
        System.out.println(S3_2.isAnagram(input1,input2));
        System.out.println(S3_3.isAnagram(input1,input2));

    }

}
