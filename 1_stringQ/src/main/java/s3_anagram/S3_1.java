package s3_anagram;

import java.util.Arrays;

public class S3_1 {

    /*
        Using BitMap
     */

    public static boolean isAnagram(String input1, String input2) {
        int[] bitMap1 = new int[26];
        int[] bitMap2 = new int[26];

        // NOTE : Length check is needed to make sure 'ab' and 'aab' are not considered anagram

        if (input1.length() != input2.length())
            return false;

        int[] input1Bitmap = populateBitMap(bitMap1, input1.toLowerCase());
        int[] input2Bitmap = populateBitMap(bitMap2, input2.toLowerCase());
        return Arrays.equals(input1Bitmap, input2Bitmap) ? true : false;
    }


    private static int[] populateBitMap(int[] bitMap, String input) {

        for (char c : input.toCharArray()) {
            bitMap[c - 'a']++;
        }
        return bitMap;
    }
}
