package s3_anagram;

import java.util.Arrays;

public class S3_3 {

/*
    Method 3 - Count Characters

This method assumes that the set of possible characters in both strings is small. In the following implementation,
it is assumed that the characters are stored using 8 bit and there can be 256 possible characters.
*/
    private static final int NO_OF_CHARS = 256;

    public static boolean isAnagram(String input1, String input2) {
        int [] count1 =  new int[NO_OF_CHARS];
        int [] count2 =  new int[NO_OF_CHARS];


        Arrays.fill(count1, 0);
        Arrays.fill(count2, 0);

        if(input1.length()!= input2.length())
            return false;

        // Note : No need to compare the case of letter here

        char[] ch1 = input1.toCharArray();
        char[] ch2 = input2.toCharArray();


        for(int i =0; i < input1.length(); i++)
        {
            count1[ch1[i]]++;
            count2[ch2[i]]++;
        }

        return Arrays.equals(count1, count2) ? true : false;
    }

}
